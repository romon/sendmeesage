package net.cashier.banksms.biz;

import net.cashier.banksms.unbiz.bean.Bean;

import java.io.Serializable;

/**
 * @author: Nixon
 * @ProjectName: CashierSmsAndroid
 * @Package: net.cashier.banksms.biz
 * @ClassName: Sms
 * @CreateDate: 2020/8/25 14:19
 * @Description: 本类作用描述：
 * @UpdateUser: 更新者：
 * @UpdateRemark: 更新说明：
 */
public class Sms extends Bean implements Serializable {
    private String account;
    private String sms;

    public Sms(String account, String sms) {
        this.account = account;
        this.sms = sms;
    }

    public String getAccount() {
        return account == null ? "" : account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getSms() {
        return sms == null ? "" : sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }
}
