package net.cashier.banksms.biz;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author: Nixon
 * @ProjectName: CashierSmsAndroid
 * @Package: net.cashier.banksms.biz
 * @ClassName: MessageEvent
 * @CreateDate: 2020/8/25 14:19
 * @Description: 本类作用描述：
 * @UpdateUser: 更新者：
 * @UpdateRemark: 更新说明：
 */
public class MessageEvent {
    private Sms sms;
    private boolean success;
    private String message;
    private String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    private String tag;

    public MessageEvent(Sms sms, boolean success) {
        this.sms = sms;
        this.success = success;
    }

    public MessageEvent(Sms sms, String message, String tag) {
        this.sms = sms;
        this.message = message;
        this.tag = tag;
    }

    public String getTag() {
        return tag == null ? "" : tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Sms getSms() {
        return sms;
    }

    public void setSms(Sms sms) {
        this.sms = sms;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message == null ? "" : message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time == null ? "" : time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
