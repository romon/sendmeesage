package net.cashier.banksms.camera;

/**
 * @author: Nixon
 * @ProjectName: CashierSmsAndroid
 * @Package: net.cashier.banksms.camera
 * @ClassName: ResultCallBack
 * @CreateDate: 2020/9/29 15:34
 * @Description: 本类作用描述：
 * @UpdateUser: 更新者：
 * @UpdateRemark: 更新说明：
 */
public interface ResultCallBack {
    void resultCallBack(String msg);
}
