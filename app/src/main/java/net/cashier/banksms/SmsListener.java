package net.cashier.banksms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import net.cashier.banksms.biz.MessageEvent;
import net.cashier.banksms.biz.Sms;
import net.cashier.banksms.unbiz.util.Http;
import net.cashier.banksms.unbiz.util.Preferences;

import org.greenrobot.eventbus.EventBus;

import static net.cashier.banksms.MainActivity.ACCOUNT;
import static net.cashier.banksms.MainActivity.API;
import static net.cashier.banksms.MainActivity.ENV;
import static net.cashier.banksms.MainActivity.STAGING;
import static net.cashier.banksms.MainActivity.TEST_URL;
import static net.cashier.banksms.MainActivity.URL;

/**
 * @author: Nixon
 * @ProjectName: CashierSmsAndroid
 * @Package: net.cashier.banksms
 * @ClassName: SmsListener
 * @CreateDate: 2020/8/25 14:09
 * @Description: 本类作用描述：
 * @UpdateUser: 更新者：
 * @UpdateRemark: 更新说明：
 */
public class SmsListener extends BroadcastReceiver {
    private static final String TAG = "SMS";
    public static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";
    private Http http = new Http();
    private String progress = "";
    private MessageEvent executeProgress = null;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(SMS_RECEIVED_ACTION)) {
            StringBuilder body = new StringBuilder();
            SmsMessage smsMessage = null;
            if (null != intent.getExtras()) {
                try {
                    Object[] pdus = (Object[]) intent.getExtras().get("pdus");
                    for (Object pdu : pdus) {
                        smsMessage = SmsMessage.createFromPdu((byte[]) pdu);
                        String msgBosy = smsMessage.getMessageBody();
                        body.append(msgBosy);
                    }
                    progress = "上报的短信内容：\n第一步：\n" + body.toString() + "\n\n";
                    String account = Preferences.get(SmsApplication.getInstance(), ACCOUNT, null);
                    executeProgress = new MessageEvent(new Sms(account, body.toString()), progress, "Tag");
                    EventBus.getDefault().post(executeProgress);
                    send(body.toString());
                } catch (Exception e) {
                    Log.e(TAG, e.getLocalizedMessage());
                }
            }
        }
    }

    public void send(String content) {
        SmsApplication app = SmsApplication.getInstance();
        String account = Preferences.get(app, ACCOUNT, null);
        if (null == account) {
            Toast.makeText(app, "银行卡号为空", Toast.LENGTH_SHORT).show();
            progress = progress + "第二步：\n银行卡号为空\n\n";
            executeProgress.setMessage(progress);
            EventBus.getDefault().post(executeProgress);
            return;
        }
        String env = Preferences.get(app, ENV, null);
        if (null == env) {
            Toast.makeText(app, "当前环境未确定", Toast.LENGTH_SHORT).show();
            progress = progress + "第二+1步：\n当前环境未确定，请配置环境链接\n\n";
            executeProgress.setMessage(progress);
            EventBus.getDefault().post(executeProgress);
            return;
        }

        Sms sms = new Sms(account, content);
        if (STAGING.equals(env)) {
            String url = Preferences.get(app, TEST_URL, null);
            if (url == null || url.isEmpty() || url.trim().length() == 0) {
                Toast.makeText(app, "请配置测试环境地址", Toast.LENGTH_SHORT).show();
                progress = progress + "第三步：\n请配置测试环境地址\n\n";
                executeProgress.setMessage(progress);
                EventBus.getDefault().post(executeProgress);
                return;
            }
            progress = progress + "第三+1步：\n开始执行短信发送流程：url==" + url + API + "，sms：" + sms.toString() + "\n\n";
            EventBus.getDefault().post(progress);
            http.sendSms(url + API, sms, progress);
        } else {
            String url = Preferences.get(app, URL, null);
            if (url == null || url.isEmpty() || url.trim().length() == 0) {
                Toast.makeText(app, "请配置运营环境地址", Toast.LENGTH_SHORT).show();
                progress = progress + "第四步：\n请配置运营环境地址\n\n";
                executeProgress.setMessage(progress);
                EventBus.getDefault().post(executeProgress);
                return;
            }
            progress = progress + "第四+1步：\n开始执行短信发送流程：url==" + url + API + "，sms：" + sms.toString() + "\n\n";
            executeProgress.setMessage(progress);
            EventBus.getDefault().post(executeProgress);
            http.sendSms(url + API, sms, progress);
        }
    }
}
