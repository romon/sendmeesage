package net.cashier.banksms;

import android.app.Application;

import net.cashier.banksms.unbiz.util.Http;

import androidx.multidex.MultiDex;

/**
 * @author: Nixon
 * @ProjectName: CashierSmsAndroid
 * @Package: net.cashier.banksms
 * @ClassName: SmsApplication
 * @CreateDate: 2020/8/25 15:17
 * @Description: 本类作用描述：
 * @UpdateUser: 更新者：
 * @UpdateRemark: 更新说明：
 */
public class SmsApplication extends Application {
    private static SmsApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        MultiDex.install(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
//        Http mHttp = new Http();
//        if (mHttp.executor.isShutdown()){
//            mHttp.executor.shutdown();
//        }
    }

    public static SmsApplication getInstance() {
        return instance;
    }
}
