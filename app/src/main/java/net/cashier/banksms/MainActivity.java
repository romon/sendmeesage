package net.cashier.banksms;

import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.permissionx.guolindev.PermissionX;
import com.permissionx.guolindev.callback.ExplainReasonCallbackWithBeforeParam;
import com.permissionx.guolindev.callback.ForwardToSettingsCallback;
import com.permissionx.guolindev.callback.RequestCallback;
import com.permissionx.guolindev.request.ExplainScope;
import com.permissionx.guolindev.request.ForwardScope;

import net.cashier.banksms.biz.MessageEvent;
import net.cashier.banksms.unbiz.util.IgnoreBatteryOptimization;
import net.cashier.banksms.unbiz.util.Preferences;
import net.cashier.banksms.unbiz.util.Strings;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    //控制是商业版本还是运营环境版本
    //    public static boolean isBusiness = true;
    private static final boolean IS_ONLINE = true;
    public static final String URL = "URL";
    public static final String TEST_URL = "TEST_URL";
    public static final String ACCOUNT = "ACCOUNT";
    public static final String ENV = "ENV";
    public static final String STAGING = "STAGING";
    public static final String ONLINE = "ONLINE";//'6236 6831 1000 6588 582

    public static final String STAGING_URL = "https://uatcashierapi.drippay.net";//测试环境地址
    //    public static final String ONLINE_URL = "https://cashierapi.drippay.net";//运营环境地址
    //isBusiness = true：商业环境地址；isBusiness = false：运营环境地址
    public static String ONLINE_URL = "https://cashierapi.drippay.net";

    public static final String API = "/bankai/bot/upload_verification";

    private String empty = "";

    TextView hint;
    EditText cardEdit;
    EditText urlEdit;
    EditText testUrlEdit;

    TextView head;
    TextView body;
    TextView msg;
    TextView tvInfo;

    RadioGroup rgUrl;
    RadioButton rbOperation, rbBusiness, rbUat;
    String TAG = "TAGTAG";

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        EventBus.getDefault().register(this);

        requestPermission();
        init();
    }

    private void requestPermission() {
        List<String> permissionList = new ArrayList<>();
        permissionList.add(Manifest.permission.READ_SMS);
        permissionList.add(Manifest.permission.RECEIVE_SMS);
        permissionList.add(Manifest.permission.SEND_SMS);
        permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissionList.add(Manifest.permission.CAMERA);

        PermissionX.init(this)
                .permissions(permissionList)
                .onExplainRequestReason(new ExplainReasonCallbackWithBeforeParam() {
                    @Override
                    public void onExplainReason(ExplainScope scope, List<String> deniedList, boolean beforeRequest) {
                        scope.showRequestReasonDialog(deniedList, "短信上报核心功能需要这些权限", "OK", "Cancel");
                    }
                })
                .onForwardToSettings(new ForwardToSettingsCallback() {
                    @Override
                    public void onForwardToSettings(ForwardScope scope, List<String> deniedList) {
                        Log.e("TAGTAG", "您需要在设置中手动允许必要的权限：" + deniedList);
                        scope.showForwardToSettingsDialog(deniedList, "您需要在设置中手动允许必要的权限", "OK", "Cancel");
                    }
                })
                .request(new RequestCallback() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResult(boolean allGranted, List<String> grantedList, List<String> deniedList) {
                        IgnoreBatteryOptimization.ignoreBatteryOptimizations(MainActivity.this);
                        Log.e("TAGTAG", "onResult: allGranted===" + allGranted);
                        Log.e("TAGTAG", "onResult: grantedList===" + grantedList);
                        Log.e("TAGTAG", "onResult: deniedList===" + deniedList);
                        if (!allGranted) {
                            String text = "您拒绝了关键权限: " + deniedList + "，应用将无法正常工作";
                            Toast.makeText(MainActivity.this, text, Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void init() {
        hint = findViewById(R.id.env);
        cardEdit = findViewById(R.id.card);
        urlEdit = findViewById(R.id.url);
        testUrlEdit = findViewById(R.id.test_url);

        head = findViewById(R.id.sms_head);
        body = findViewById(R.id.sms_body);
        msg = findViewById(R.id.sms_msg);

        rgUrl = findViewById(R.id.rgUrl);
        rbBusiness = findViewById(R.id.rbBusiness);
        rbOperation = findViewById(R.id.rbOperation);
        rbUat = findViewById(R.id.rbUat);
        tvInfo = findViewById(R.id.tvInfo);

        cardEdit.setText(Preferences.get(this, ACCOUNT, empty));

        String urlEnv = Preferences.get(this, URL, ONLINE_URL);

        urlEdit.setText(urlEnv);
        if (urlEnv.equals("https://uatcashierapi.drippay.net")) {
            rbUat.setChecked(true);
        }
        if (urlEnv.equals("https://cashierapi.drippay.net")) {
            rbOperation.setChecked(true);
        }

        if (IS_ONLINE) {
            testUrlEdit.setVisibility(View.GONE);
            findViewById(R.id.test_label).setVisibility(View.GONE);
            findViewById(R.id.toggle).setVisibility(View.GONE);
        } else {
            testUrlEdit.setText(Preferences.get(this, TEST_URL, STAGING_URL));
        }

        String env = Preferences.get(this, ENV, IS_ONLINE ? ONLINE : STAGING);
        if (env != null && !env.isEmpty() && env.trim().length() != 0) {
            hint.setText("当前为" + (env.equals(STAGING) ? "测试" : "运营") + "环境");
        }

        rgUrl.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                String url = "";
                switch (checkedId) {
                    case R.id.rbBusiness:
                        url = rbBusiness.getText().toString();
                        urlEdit.setText(url);
                        break;
                    case R.id.rbOperation:
                        url = rbOperation.getText().toString();
                        urlEdit.setText(url);
                        break;
                    case R.id.rbUat:
                        url = rbUat.getText().toString();
                        urlEdit.setText(url);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    //toggle按钮-切换环境-的点击事件
    public void toggle(View view) {
        String env = Preferences.get(this, ENV, empty);
        String url = Preferences.get(this, URL, empty);
        String testUrl = Preferences.get(this, TEST_URL, empty);

        if (isNull(env) && isNull(url) && isNull(testUrl)) {
            hint.setText("请先完善环境配置");
            return;
        } else if (isNull(env) && isNull(testUrl)) {
            env = STAGING;
        } else if (isNull(env)) {
            env = ONLINE;
        } else {
            env = env.equals(STAGING) ? ONLINE : STAGING;
        }
        hint.setText("当前为" + (env.equals(STAGING) ? "测试" : "运营") + "环境");
        Preferences.set(this, ENV, env);
    }

    //save按钮-保存配置-的点击事件
    public void save(View view) {
        String account = cardEdit.getText().toString();
        if (isNull(account)) {
            Toast.makeText(this, "请先绑定银行卡", Toast.LENGTH_SHORT).show();
            return;
        }
        String testUrl = testUrlEdit.getText().toString();
        String url = urlEdit.getText().toString();
        if (isNull(testUrl) && isNull(url)) {
            Toast.makeText(this, "请填写测试环境或运营环境的地址", Toast.LENGTH_SHORT).show();
            return;
        }

        boolean mACCOUNT = Preferences.set(this, ACCOUNT, Strings.removeSpace(account));
        boolean mURL = Preferences.set(this, URL, Strings.removeSpace(url));
        boolean mTEST_URL = Preferences.set(this, TEST_URL, Strings.removeSpace(testUrl));
        boolean mENV = Preferences.set(this, ENV, IS_ONLINE ? ONLINE : STAGING);
        if (mACCOUNT && mURL && mTEST_URL && mENV) {
            Toast.makeText(this, "保存成功", Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (executeSave) {
            executeSave = false;
            save(new View(this));
            Log.e(TAG, "executeProgress: " + "这里又再次的执行了保存环境的方法");
        }
        if ("Tag".equals(event.getTag())) {
            tvInfo.setText("短信上报执行步骤：\n" + event.getMessage());
            return;
        }
        head.setText("时间" + event.getTime() + "    状态：上报" + event.isSuccess());
        body.setText(event.getSms().getSms());
        msg.setText(event.getMessage());
    }

    private boolean executeSave = true;
    //    @Subscribe(threadMode = ThreadMode.MAIN)
    //    public void executeProgress(String progress) {
    //        if (executeSave){
    //            executeSave = false;
    //            save(new View(this));
    //            Log.e(TAG, "executeProgress: " + "这里又再次的执行了保存环境的方法");
    //        }
    //        tvInfo.setText("短信上报执行步骤：\n" + progress);
    //    }

    private boolean isNull(String s) {
        if (null == s || s.isEmpty() || s.trim().length() == 0) {
            return true;
        }
        return false;
    }

    //屏幕常亮开
    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    //屏幕常亮关
    @Override
    protected void onPause() {
        super.onPause();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}