package net.cashier.banksms.unbiz.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.RequiresApi;

/**
 * @author: Nixon
 * @ProjectName: MobilePhoneAssistant
 * @Package: com.mobile.automatic.utils
 * @ClassName: IgnoreBatteryOptimization
 * @CreateDate: 2020/10/13 12:01
 * @Description: 本类作用描述：将应用加入白名单以及忽略电池优化
 * @UpdateUser: 更新者：
 * @UpdateRemark: 更新说明：
 */
public class IgnoreBatteryOptimization {
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void ignoreBatteryOptimizations(Context mContext) {
        boolean isIgnoreBattery = isIgnoreBatteryOptimizations(mContext);
        Log.e("TAGTAG", "onCreate: isIgnoreBattery===" + isIgnoreBattery);
        if (!isIgnoreBattery) {
            Log.e("TAGTAG", Build.BRAND.toLowerCase());
            requestIgnoreBatteryOptimizations(mContext);
            prompt(mContext);
        }
    }

    //检查应用是否在白名单里面
    @RequiresApi(api = Build.VERSION_CODES.M)
    private static boolean isIgnoreBatteryOptimizations(Context mContext) {
        boolean isignore = false;
        PowerManager powerManager = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        if (null != powerManager) {
            isignore = powerManager.isIgnoringBatteryOptimizations(mContext.getPackageName());
        }
        return isignore;
    }

    //跳往白名单设置界面
    private static void requestIgnoreBatteryOptimizations(Context mContext) {
        try {
            Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + mContext.getPackageName()));
            mContext.startActivity(intent);
            Log.e("TAGTAG", "requestIgnoreBatteryOptimizations: requestIgnoreBatteryOptimizations");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //跳往电池管理界面，运行应用能够高耗电时允许其继续运行
    @RequiresApi(api = Build.VERSION_CODES.M)
    private static void prompt(Context mContext) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
        dialog.setTitle("温馨提示")
                .setMessage("请在应用自启动管理里面开启应用的自动管理，或者在电池管理里面的后台高耗电列表里面允许本应用高耗电时继续运行")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SkipUtils.getInstance().skipToWhiteList(mContext, Build.BRAND.toLowerCase());
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }
}
