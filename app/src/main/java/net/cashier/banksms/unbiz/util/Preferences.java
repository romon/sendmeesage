package net.cashier.banksms.unbiz.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * @author: Nixon
 * @ProjectName: CashierSmsAndroid
 * @Package: net.cashier.banksms.unbiz.util
 * @ClassName: Preferences
 * @CreateDate: 2020/8/25 15:27
 * @Description: 本类作用描述：
 * @UpdateUser: 更新者：
 * @UpdateRemark: 更新说明：
 */
public class Preferences {
    public static final String preferences_file_key = "cashier_bank_sms";

    public static boolean set(Context context, String key, String value) {
        SharedPreferences preferences = context.getSharedPreferences(preferences_file_key, Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public static String get(Context context, String key, String defaultValue) {
        SharedPreferences preferences = context.getSharedPreferences(preferences_file_key, Context.MODE_PRIVATE);
        return preferences.getString(key, defaultValue);
    }


}
