package net.cashier.banksms.unbiz.util;

import android.os.Environment;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * @author: Nixon
 * @ProjectName: CashierSmsAndroid
 * @Package: net.cashier.banksms.unbiz.util
 * @ClassName: Logger
 * @CreateDate: 2020/8/25 15:19
 * @Description: 本类作用描述：
 * @UpdateUser: 更新者：
 * @UpdateRemark: 更新说明：
 */
public class Logger {
    private static final String PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
    private static final String LOG = PATH + "LOG.txt";

    public static void printStackTrace(Exception e){
        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        try {
            File dir = new File(PATH);
            if (!dir.exists()){
                dir.mkdirs();
            }

            File file = new File(LOG);
            if (!file.exists()){
                file.createNewFile();
            }
            fileWriter = new FileWriter(LOG, true);
            printWriter = new PrintWriter(fileWriter);
            e.printStackTrace(printWriter);
        }catch (Exception e1){
            e1.printStackTrace();
        }finally {
            try {
                fileWriter.close();
                printWriter.close();
            }catch (Exception e2){
                e2.printStackTrace();
            }
        }
    }
}
