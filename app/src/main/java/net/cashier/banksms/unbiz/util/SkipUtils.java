package net.cashier.banksms.unbiz.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;

/**
 * @author: Nixon
 * @ProjectName: CashierSmsAndroid
 * @Package: net.cashier.banksms.unbiz.util
 * @ClassName: SkipUtils
 * @CreateDate: 2020/9/29 11:25
 * @Description: 本类作用描述：
 * @UpdateUser: 更新者：
 * @UpdateRemark: 更新说明：
 */
public class SkipUtils {

    public static SkipUtils getInstance() {
        return Singleton.INSTANCE;
    }

    private static class Singleton {
        public static SkipUtils INSTANCE = new SkipUtils();
    }

    public void skipToWhiteList(@NonNull Context mContext, String platform) {
        if (null == platform || platform.length() == 0 || platform.trim().length() == 0) {
            return;
        }
        switch (platform) {
            case "huawei":
                goHuaweiSetting(mContext);
                break;
            case "honor":
                goHuaweiSetting(mContext);
                break;
            case "xiaomi":
                goXiaomiSetting(mContext);
                break;
            case "oppo":
                goOPPOSetting(mContext);
                break;
            case "vivo":
                goVIVOSetting(mContext);
                break;
            case "meizu":
                goMeizuSetting(mContext);
                break;
            case "samsung":
                goSamsungSetting(mContext);
                break;
            case "letv":
                goLetvSetting(mContext);
                break;
            case "smartisan":
                goSmartisanSetting(mContext);
                break;
            default:
                break;
        }
    }

    /**
     * 华为
     *
     * @param mContext
     */
    private void goHuaweiSetting(@NonNull Context mContext) {
        if (isHuawei()) {
            try {
                showActivity(mContext, "com.huawei.systemmanager",
                        "com.huawei.systemmanager.startupmgr.ui.StartupNormalAppListActivity");
            } catch (Exception e) {
                showActivity(mContext, "com.huawei.systemmanager",
                        "com.huawei.systemmanager.optimize.bootstart.BootStartActivity");
            }
        }
    }

    private boolean isHuawei() {
        return Build.BRAND != null && (Build.BRAND.toLowerCase().equals("huawei") || Build.BRAND.toLowerCase().equals("honor"));
    }

    /**
     * 小米
     *
     * @param mContext
     */
    private void goXiaomiSetting(@NonNull Context mContext) {
        try {
            if (isXiaomi()) {
                showActivity(mContext, "com.miui.securitycenter",
                        "com.miui.permcenter.autostart.AutoStartManagementActivity");
            }
        } catch (Exception e) {

        }
    }

    private boolean isXiaomi() {
        return Build.BRAND != null && Build.BRAND.toLowerCase().equals("xiaomi");
    }

    /**
     * OPPO
     * 权限隐私 -> 自启动管理 -> 允许应用自启动。
     *
     * @return
     */
    private void goOPPOSetting(@NonNull Context mContext) {
        if (isOPPO()) {
            try {
                showActivity(mContext, "com.coloros.phonemanager");
            } catch (Exception e1) {
                try {
                    showActivity(mContext, "com.oppo.safe");
                } catch (Exception e2) {
                    try {
                        showActivity(mContext, "com.coloros.oppoguardelf");
                    } catch (Exception e3) {
                        showActivity(mContext, "com.coloros.safecenter");
                    }
                }
            }
        }
    }

    private boolean isOPPO() {
        return Build.BRAND != null && Build.BRAND.toLowerCase().equals("oppo");
    }

    /**
     * VIVO
     *
     * @param mContext
     */
    private void goVIVOSetting(@NonNull Context mContext) {
        if (isVIVO()) {
            try {
                showActivity(mContext, "com.iqoo.secure");
            } catch (Exception e) {

            }
        }
    }

    private boolean isVIVO() {
        return Build.BRAND != null && Build.BRAND.toLowerCase().equals("vivo");
    }

    /**
     * 魅族
     *
     * @param mContext
     */
    private void goMeizuSetting(@NonNull Context mContext) {
        if (isMeizu()) {
            try {
                showActivity(mContext, "com.meizu.safe");
            } catch (Exception e) {

            }
        }
    }

    private boolean isMeizu() {
        return Build.BRAND != null && Build.BRAND.toLowerCase().equals("meizu");
    }

    /**
     * 三星
     *
     * @param mContext
     */
    private void goSamsungSetting(@NonNull Context mContext) {
        if (isSamsung()) {
            try {
                showActivity(mContext, "com.samsung.android.sm_cn");
            } catch (Exception e) {
                showActivity(mContext, "com.samsung.android.sm");
            }
        }
    }

    private boolean isSamsung() {
        return Build.BRAND != null && Build.BRAND.toLowerCase().equals("samsung");
    }

    /**
     * 乐视
     *
     * @param mContext
     */
    private void goLetvSetting(@NonNull Context mContext) {
        if (isLeTV()) {
            try {
                showActivity(mContext, "com.letv.android.letvsafe",
                        "com.letv.android.letvsafe.AutobootManageActivity");
            } catch (Exception e) {

            }
        }
    }

    private boolean isLeTV() {
        return Build.BRAND != null && Build.BRAND.toLowerCase().equals("letv");
    }

    /**
     * 锤子
     *
     * @param mContext
     */
    private void goSmartisanSetting(@NonNull Context mContext) {
        if (isSmartisan()) {
            try {
                showActivity(mContext, "com.smartisanos.security");
            } catch (Exception e) {

            }
        }
    }

    private boolean isSmartisan() {
        return Build.BRAND != null && Build.BRAND.toLowerCase().equals("smartisan");
    }

    private boolean androidPlatform(@NonNull String platform) {
        return Build.BRAND != null && Build.BRAND.toLowerCase().equals(platform);
    }

    /**
     * 跳转到指定应用的首页
     */
    private void showActivity(@NonNull Context mContext, @NonNull String packageName) {
        Intent intent = mContext.getPackageManager().getLaunchIntentForPackage(packageName);
        mContext.startActivity(intent);
    }

    /**
     * 跳转到指定应用的指定页面
     */
    private void showActivity(@NonNull Context mContext, @NonNull String packageName, @NonNull String activityDir) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(packageName, activityDir));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    //region+注销备用
    //    public void saveCurrentImage(@NonNull Activity mContext, ImageView view, ResultCallBack resultCallBack) {
    //        //获取当前屏幕的大小
    //        int width = mContext.getWindow().getDecorView().getRootView().getWidth();
    //        int height = mContext.getWindow().getDecorView().getRootView().getHeight();
    //        //生成相同大小的图片
    //        Bitmap temBitmap;
    //        //找到当前页面的根布局
    //        //        View view = mContext.getWindow().getDecorView().getRootView();
    //        //设置缓存
    //        view.setDrawingCacheEnabled(true);
    //        view.buildDrawingCache();
    //        //从缓存中获取当前屏幕的图片,创建一个DrawingCache的拷贝，因为DrawingCache得到的位图在禁用后会被回收
    //        temBitmap = view.getDrawingCache();
    //        SimpleDateFormat df = new SimpleDateFormat("yyyymmddhhmmss");
    //        final String time = df.format(new Date());
    //        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
    //            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/screen";
    //            File file = new File(path, time + ".png");
    //            if (!file.exists()) {
    //                file.getParentFile().mkdirs();
    //                try {
    //                    file.createNewFile();
    //                } catch (IOException e) {
    //                    e.printStackTrace();
    //                }
    //            }
    //            FileOutputStream fos = null;
    //            try {
    //                fos = new FileOutputStream(file);
    //                temBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
    //                fos.flush();
    //                fos.close();
    //            } catch (FileNotFoundException e) {
    //                e.printStackTrace();
    //            } catch (IOException e) {
    //                e.printStackTrace();
    //            }
    //            new Thread(new Runnable() {
    //                @Override
    //                public void run() {
    //                    String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/screen/" + time + ".png";
    //                    final Result result = parseQRcodeBitmap(path);
    //                    if (null != result) {
    //                        resultCallBack.resultCallBack(result.toString());
    //                    } else {
    //                        resultCallBack.resultCallBack("无法识别");
    //                    }
    //                }
    //            }).start();
    //            //禁用DrawingCahce否则会影响性能 ,而且不禁止会导致每次截图到保存的是第一次截图缓存的位图
    //            view.setDrawingCacheEnabled(false);
    //        }
    //    }
    //
    //    //解析二维码图片,返回结果封装在Result对象中
    //    private Result parseQRcodeBitmap(String bitmapPath) {
    //        //解析转换类型UTF-8
    //        Hashtable<DecodeHintType, String> hints = new Hashtable<DecodeHintType, String>();
    //        hints.put(DecodeHintType.CHARACTER_SET, "utf-8");
    //        //获取到待解析的图片
    //        BitmapFactory.Options options = new BitmapFactory.Options();
    //        options.inJustDecodeBounds = true;
    //        Bitmap bitmap = BitmapFactory.decodeFile(bitmapPath, options);
    //        options.inSampleSize = options.outHeight / 400;
    //        if (options.inSampleSize <= 0) {
    //            options.inSampleSize = 1; //防止其值小于或等于0
    //        }
    //        options.inJustDecodeBounds = false;
    //        bitmap = BitmapFactory.decodeFile(bitmapPath, options);
    //        //新建一个RGBLuminanceSource对象，将bitmap图片传给此对象
    //        RGBLuminanceSource rgbLuminanceSource = new RGBLuminanceSource(bitmap);
    //        //将图片转换成二进制图片
    //        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(rgbLuminanceSource));
    //        //初始化解析对象
    //        QRCodeReader reader = new QRCodeReader();
    //        //开始解析
    //        Result result = null;
    //        try {
    //            result = reader.decode(binaryBitmap, hints);
    //        } catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //        return result;
    //    }
    //
    //    public class RGBLuminanceSource extends LuminanceSource {
    //        private byte bitmapPixels[];
    //        protected RGBLuminanceSource(Bitmap bitmap) {
    //            super(bitmap.getWidth(), bitmap.getHeight());
    //
    //            // 首先，要取得该图片的像素数组内容
    //            int[] data = new int[bitmap.getWidth() * bitmap.getHeight()];
    //            this.bitmapPixels = new byte[bitmap.getWidth() * bitmap.getHeight()];
    //            bitmap.getPixels(data, 0, getWidth(), 0, 0, getWidth(), getHeight());
    //
    //            // 将int数组转换为byte数组，也就是取像素值中蓝色值部分作为辨析内容
    //            for (int i = 0; i < data.length; i++) {
    //                this.bitmapPixels[i] = (byte) data[i];
    //            }
    //        }
    //
    //        @Override
    //        public byte[] getMatrix() {
    //            // 返回我们生成好的像素数据
    //            return bitmapPixels;
    //        }
    //
    //        @Override
    //        public byte[] getRow(int y, byte[] row) {
    //            // 这里要得到指定行的像素数据
    //            System.arraycopy(bitmapPixels, y * getWidth(), row, 0, getWidth());
    //            return row;
    //        }
    //    }
    //endregion
}
