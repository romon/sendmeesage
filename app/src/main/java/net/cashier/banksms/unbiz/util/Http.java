package net.cashier.banksms.unbiz.util;

import android.util.Log;

import com.google.gson.Gson;

import net.cashier.banksms.biz.MessageEvent;
import net.cashier.banksms.biz.Sms;
import net.cashier.banksms.unbiz.bean.Bean;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author: Nixon
 * @ProjectName: CashierSmsAndroid
 * @Package: net.cashier.banksms.unbiz.util
 * @ClassName: Http
 * @CreateDate: 2020/8/25 15:32
 * @Description: 本类作用描述：
 * @UpdateUser: 更新者：
 * @UpdateRemark: 更新说明：
 */
public class Http {
    public final String TAG = "TAGTAG";
    //    public final ExecutorService executor = Executors.newFixedThreadPool(3);
    public final MediaType JSON = MediaType.get("application/json;charset=utf-8");
    private String filePath = "/sdcard/Gyt/";
    private String fileName = "SendMessage.txt";
    private boolean isCirculation = false;
    private MessageEvent executeProgress = null;
    private String msg = "";
    private MessageEvent event = null;

    public synchronized void sendSms(String url, Bean bean, String progress) {
        isCirculation = false;
        isCirculation = false;
        msg = progress + "第五步：\n短信上报执行到：executor-->>Runnable--run()\n\n";
        executeProgress = new MessageEvent((Sms) bean, msg, "Tag");
        EventBus.getDefault().post(executeProgress);

        event = new MessageEvent((Sms) bean, isCirculation);

        String requestData = new Gson().toJson(bean);
        Log.e(TAG, "短信上报内容是: " + requestData);

        OkHttpClient client = SSLSocketClient.getUnsafeOkHttpClient();
        RequestBody body = RequestBody.create(requestData, JSON);
        Request request = new Request.Builder().url(url).post(body).build();
        Call call = client.newCall(request);

        msg = progress + executeProgress.getMessage() + "第六步：\n短信上报执行到：“OkHttpClient、RequestBody和Request”对象创建\n\n";
        executeProgress.setMessage(msg);
        EventBus.getDefault().post(executeProgress);
        for (int i = 0; i < 30; i++) {
            if (isCirculation) {
                msg = progress + executeProgress.getMessage() + "第七步：\n短信上报执行到：“for循环的if (isCirculation){}”判断\n\n";
                executeProgress.setMessage(msg);
                EventBus.getDefault().post(executeProgress);
                return;
            }
            Log.e(TAG, "短信上报功能的第: " + (i + 1) + "次上报");

            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String mResponse = response.body().string();
                    parseData(mResponse, progress);
                }

                @Override
                public void onFailure(Call call, IOException e) {
                    Log.d(TAG, "onFailure: ");
                    msg = progress + executeProgress.getMessage() +
                            "第十二步：\n短信上报执行到：“onFailure(Call call, IOException e)”异常：" + e.getMessage() + "\n\n";
                    executeProgress.setMessage(msg);
                    EventBus.getDefault().post(executeProgress);

                    Log.e(TAG, "上传短信异常：" + e.getMessage());
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
                    String time = df.format(new Date());// new Date()为获取当前系统时间，也可使用当前时间戳
                    FileTools.writeTxtToFile(time + "：catch (Exception e)：上传短信异常：" + e.getMessage(), filePath, fileName);
                    event.setMessage(e.getMessage());
                    EventBus.getDefault().post(event);
                }
            });

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(TAG, "sleep休眠异常");
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "sleep休眠异常");
            }
        }
    }

    private void parseData(String mResponse, String progress) {
        try {
            Log.e(TAG, "短信上报结果" + mResponse);
            JSONObject result = new JSONObject(mResponse);
            Log.e(TAG, "解密后的信息: " + result);

            msg = progress + executeProgress.getMessage() + "第八步：\n短信上报执行到：“Response”对象数据解析，result：" + result + "\n\n";
            executeProgress.setMessage(msg);
            EventBus.getDefault().post(executeProgress);
            if (result.optInt("ret", 1) == 0) {
                msg = progress + executeProgress.getMessage() +
                        "第九步：\n短信上报执行到：“if (result.optInt(ret, 1) == 0) {}”\n\n";
                executeProgress.setMessage(msg);
                EventBus.getDefault().post(executeProgress);

                Log.e(TAG, "短信上报成功");
                isCirculation = true;
                event.setSuccess(isCirculation);
                event.setMessage("短信上报成功");
                EventBus.getDefault().post(event);
            } else {
                msg = progress + executeProgress.getMessage() + "第十步：\n短信上报执行到：“if (result.optInt(ret, 1) == 0) {}else{}”\n\n";
                executeProgress.setMessage(msg);
                EventBus.getDefault().post(executeProgress);

                Log.e(TAG, "短信上报失败");
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
                String time = df.format(new Date());// new Date()为获取当前系统时间，也可使用当前时间戳
                FileTools.writeTxtToFile(time + "：短信上报失败", filePath, fileName);
                event.setMessage("短信上报失败");
                EventBus.getDefault().post(event);
            }
        } catch (Exception e) {
            msg = progress + executeProgress.getMessage() + "第十一步：\n短信上报执行到：“client.newCall(request).execute()catch (Exception e)”异常：" + e.getMessage() + "\n\n";
            executeProgress.setMessage(msg);
            EventBus.getDefault().post(executeProgress);

            Log.e(TAG, "上传短信异常：" + e.getMessage());
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
            String time = df.format(new Date());// new Date()为获取当前系统时间，也可使用当前时间戳
            FileTools.writeTxtToFile(time + "：catch (Exception e)：上传短信异常：" + e.getMessage(), filePath, fileName);
            event.setMessage(e.getMessage());
            EventBus.getDefault().post(event);
        }
    }
}
