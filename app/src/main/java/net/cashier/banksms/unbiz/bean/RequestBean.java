package net.cashier.banksms.unbiz.bean;

/**
 * @author: Nixon
 * @ProjectName: SendMeesage
 * @Package: net.cashier.banksms.unbiz.bean
 * @ClassName: RequestBean
 * @CreateDate: 2020/10/17 18:44
 * @Description: 本类作用描述：
 * @UpdateUser: 更新者：
 * @UpdateRemark: 更新说明：
 */
public class RequestBean {

    /**
     * ret : 0
     * msg : null
     * Data : null
     */
    private int ret;
    private String msg;
    private Object Data;

    public int getRet() {
        return ret;
    }

    public void setRet(int ret) {
        this.ret = ret;
    }

    public String getMsg() {
        return msg == null ? "" : msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return Data;
    }

    public void setData(Object data) {
        Data = data;
    }
}
