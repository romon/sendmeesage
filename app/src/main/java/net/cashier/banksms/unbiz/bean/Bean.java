package net.cashier.banksms.unbiz.bean;

import android.util.Log;

import net.cashier.banksms.unbiz.util.Strings;

import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author: Nixon
 * @ProjectName: CashierSmsAndroid
 * @Package: net.cashier.banksms.unbiz.bean
 * @ClassName: Bean
 * @CreateDate: 2020/8/25 14:20
 * @Description: 本类作用描述：
 * @UpdateUser: 更新者：
 * @UpdateRemark: 更新说明：
 */
public class Bean implements Serializable {

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder("{");
        Class cls = this.getClass();
        while (cls != Bean.class){
            Field[] fields = cls.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                if (i != 0){
                    builder.append(",");
                }
                String name = fields[i].getName();
                try{
                    Method method = this.getClass().getMethod(Strings.buildGetter(name));
                    builder.append(name).append(method.invoke(this));
                }catch (Exception e){
                    Log.e("TAGTAG", e.getLocalizedMessage());
                }
            }
            cls = cls.getSuperclass();
        }
        builder.append("}");
        return builder.toString();
    }

    public JSONObject toJSONObject(){
        JSONObject json = new JSONObject();
        Class cls = this.getClass();
        while (cls != Bean.class){
            Field[] fields = cls.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                String name = fields[i].getName();
                try{
                    Method method = this.getClass().getMethod(Strings.buildGetter(name));
                    Object object = method.invoke(this);
                    if (object instanceof Bean){
                        json.put(name, ((Bean) object).toJSONObject());
                    }else {
                        json.put(name, method.invoke(this));
                    }
                }catch (Exception e){
                    Log.e("TAGTAG", e.getLocalizedMessage());
                }
            }
            cls = cls.getSuperclass();
        }
        return json;
    }
}
