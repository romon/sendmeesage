package net.cashier.banksms.unbiz.util;

import java.util.Random;

/**
 * @author: Nixon
 * @ProjectName: CashierSmsAndroid
 * @Package: net.cashier.banksms.unbiz.util
 * @ClassName: Strings
 * @CreateDate: 2020/8/25 14:25
 * @Description: 本类作用描述：
 * @UpdateUser: 更新者：
 * @UpdateRemark: 更新说明：
 */
public class Strings {
    public static final String WHITESPACE = "\\T\\N\\R\\V\\F";
    public static final String ASCII_LOWERCASE = "abcdefghijklmnopqrstuvwxyz";
    public static final String ASCII_UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String ASCII_LETTERS = ASCII_LOWERCASE + ASCII_UPPERCASE;
    public static final String DIGITS = "0123456789";
    public static final String HEX_DIGITS = DIGITS + "abcdef" + "ABCDEF";
    public static final String OCT_DIGITS = "01234567";
    public static final String PUNCTUAION = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
    public static final String PRINTABLE = DIGITS + ASCII_LETTERS + PUNCTUAION + WHITESPACE;

    public static String initialToUppercase(String text){
        return text.substring(0, 1).toUpperCase() + text.substring(1);
    }

    public static String buildGetter(String name){
        return "get" + Strings.initialToUppercase(name);
    }

    public static String randomDigits(int len){
        Random rano = new Random();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < len; i++) {
            builder.append(DIGITS.charAt(rano.nextInt(DIGITS.length())));
        }
        return builder.toString();
    }

    public static String randomLetters(int len){
        Random rano = new Random();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < len; i++) {
            builder.append(ASCII_LETTERS.charAt(rano.nextInt(ASCII_LETTERS.length())));
        }
        return builder.toString();
    }

    public static String randomString(int len){
        String seed = ASCII_LETTERS + DIGITS;
        Random rano = new Random();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < len; i++) {
            builder.append(seed.charAt(rano.nextInt(seed.length())));
        }
        return builder.toString();
    }

    public static String removeSpace(String text){
        if (text == null){
            return null;
        }
        return text.replace(" ", "");
    }
}
